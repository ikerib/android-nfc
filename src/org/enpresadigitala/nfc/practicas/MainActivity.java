package org.enpresadigitala.nfc.practicas;

import java.util.ArrayList;

import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends ListActivity{
	private NfcAdapter nfcAdapter;
	private NdefRecordAdapter adapter;
	private PendingIntent pendingIntent;
	private IntentFilter[] intentFiltersArray;
	private String[][] techListsArray;
	private ArrayList<NdefRecord> ndeflist= new ArrayList<NdefRecord>();
	
	@Override
    public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		uiBinds();
		//TODO
		nfcAdapter = NfcAdapter.getDefaultAdapter(this);
		pendingIntent = PendingIntent.getActivity(
				this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
			try {
				ndef.addDataType("*/*");
			} catch (MalformedMimeTypeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		intentFiltersArray = new IntentFilter[]{ndef,};
		techListsArray = new String[][]{
					new String[]{
							Ndef.class.getName(),
							}
				};
	}
	
	@Override
	public void onResume(){
		super.onResume();
		nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
	}
	
	@Override
	public void onPause(){
		super.onPause();
		nfcAdapter.disableForegroundDispatch(this);
	}
	
	@Override
	public void onNewIntent(Intent intent){
		setIntent(intent);
		resolveIntent();
	}

	public void resolveIntent(){
		if (getIntent().getAction().equals(NfcAdapter.ACTION_NDEF_DISCOVERED)) {
			NdefMessage[] msgs = null;
			Parcelable[] rawMsgs = getIntent().getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			if (rawMsgs != null){
				msgs = new NdefMessage[rawMsgs.length];
				for (int i = 0 ; i< rawMsgs.length;i++){
					msgs[i]= (NdefMessage) rawMsgs[i];
					for(NdefRecord rec : msgs[i].getRecords()){
						adapter.add(rec);
					}
				}

			}
		}
	}
	
	public void uiBinds(){
		adapter = new NdefRecordAdapter(this, R.layout.row, ndeflist);
		setListAdapter(adapter);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(this, ShareActivity.class);
		intent.putExtra("mensaje", ndeflist.get(position).toByteArray());
		startActivity(intent);
	}
}