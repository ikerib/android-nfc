package org.enpresadigitala.nfc.practicas;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.content.Context;
import android.nfc.NdefRecord;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NdefRecordAdapter extends ArrayAdapter<NdefRecord> {

    private ArrayList<NdefRecord> items;
    private Context context;
    public NdefRecordAdapter(Context context, int textViewResourceId, ArrayList<NdefRecord> items) {
            super(context, textViewResourceId, items);
            this.items = items;
            this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row, null);
            }
            NdefRecord o = items.get(position);
            if (o != null) {
                    TextView tt = (TextView) v.findViewById(R.id.toptext);
                    TextView bt = (TextView) v.findViewById(R.id.bottomtext);
                    ImageView ico = (ImageView) v.findViewById(R.id.icon);
                    if (o.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
                        byte type = o.getType()[0];
	                    if (type == NdefRecord.RTD_URI[0]) {
	                    	  
	                          try {
								tt.setText(new String(o.getPayload(),"UTF-8"));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}    
	                          bt.setText("TNF_WELL_KNOWN: RTD URI");
	                          ico.setImageResource(R.drawable.uri_icon);
	                    }
	                    else if(type == NdefRecord.RTD_TEXT[0]) {
	                          try {
								tt.setText(new String(o.getPayload(),"UTF-8"));
							} catch (UnsupportedEncodingException e) {
								e.printStackTrace();
							}    
	                          bt.setText("TNF_WELL_KNOWN: RTD TEXT");
	                          ico.setImageResource(R.drawable.text_icon);
	                    }
                    } else if (o.getTnf() == NdefRecord.TNF_MIME_MEDIA) {
                        try {
							tt.setText(new String(o.getType(),"UTF-8"));
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}                      
                        bt.setText("TNF_MIME_MEDIA: ");
                        ico.setImageResource(R.drawable.mime_icon);
                    }
                    else{
                        tt.setText("OTRO ");                            
                        bt.setText("NDEF ");
                        ico.setImageResource(R.drawable.other_icon);
                    }
            }
            return v;
    }
}