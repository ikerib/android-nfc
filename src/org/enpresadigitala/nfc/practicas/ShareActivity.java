package org.enpresadigitala.nfc.practicas;

import android.app.Activity;
import android.content.Intent;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.widget.Toast;
import android.nfc.*;
import android.nfc.tech.Ndef;

public class ShareActivity extends Activity implements NfcAdapter.CreateNdefMessageCallback, NfcAdapter.OnNdefPushCompleteCallback  {
	NdefRecord ndefRec;
    @Override
    public void onCreate(Bundle icicle){
    	super.onCreate(icicle);
	   	setContentView(R.layout.share);
	   	try {
			ndefRec= new NdefRecord(getIntent().getByteArrayExtra("mensaje"));
		} catch (FormatException e) {
			e.printStackTrace();
		}
	   	
	   	NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
	   	nfcAdapter.setNdefPushMessageCallback(this, this);
	   	nfcAdapter.setOnNdefPushCompleteCallback(this, this);	   	
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//TODO
	}
	
	@Override
	public void onResume() {
		super.onResume();
		// TODO
		}
	
	@Override
    public void onNewIntent(Intent intent) {
		// TODO
	}

	public NdefMessage createNdefMessage(NfcEvent event) {
		// TODO Auto-generated method stub
		NdefMessage msg =null;
		
		try {
			msg = new NdefMessage(ndefRec.toByteArray());
		} catch (FormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return msg;
	}

	public void onNdefPushComplete(NfcEvent event) {
		// TODO Auto-generated method stub
		Toast.makeText(this.getApplicationContext(), "ok", Toast.LENGTH_LONG);
		
	}
}
